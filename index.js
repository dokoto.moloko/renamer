/*
 * $> node index.js /Volumes/almacen/VIDEO/SERIES/L/Los\ Simpsons/TEMPORADA06
 */

const fs = require('fs');
const { Confirm } = require('enquirer');
const get = require('lodash/get');

function sourceFolderExist(folder) {
  return fs.existsSync(folder);
}

function createFolderIfExist(folder) {
  if (!fs.existsSync(`${folder}/renamedFiles`)) {
    fs.mkdirSync(`${folder}/renamedFiles`);
  }
}

// /Volumes/almacen/VIDEO/SERIES/L/Los Simpsons/TEMPORADA06/Los Simpson - Temporada 6 [DVDRIP][cap.601][Spanish][www.pctestrenos.com].avi
function martcherS01(file) {
  if (!file) return null;
  const matches = file.match(/(\d)(\d\d)/);
  if (!matches) return;
  const ext = file.match(/\.[a-z][a-z][a-z]$/);  
  const epi = get(matches, '[1]');
  const season = get(matches, '[2]');
  return {
    source: !epi || ! season ? `[ERROR] source ${file} Temporada ${season} Episodio: ${epi}` : file,
    season,
    epi, 
    ext,
  };
}

// /Volumes/almacen/VIDEO/SERIES/L/Los Simpsons/TEMPORADA01/Los Simpson - 1x02 - Bart, El Genio[Dvd-Rip][Spanish][www.zonatorrent.com].avi
function martcherS02(file) {
  if (!file) return null;
  const matches = file.match(/(\d)x(\d\d)/);
  if (!matches) return;
  const ext = file.match(/\.[a-z][a-z][a-z]$/);  
  const epi = get(matches, '[2]');
  const season = get(matches, '[1]');
  return {
    source: !epi || ! season ? `[ERROR] source ${file} Temporada ${season} Episodio: ${epi}` : file,
    season,
    epi, 
    ext,
  };
}

// 26-01.mp4
function martcherS03(file) {
  if (!file) return null;
  const matches = file.match(/(\d?\d)-(\d\d)/);
  if (!matches) return;
  const extMatches = file.match(/\.(mp4|avi|mkv)$/);  
  const ext = get(extMatches, '[0]');
  const epi = get(matches, '[2]');
  const season = get(matches, '[1]');
  return {
    source: !epi || ! season ? `[ERROR] source ${file} Temporada ${season} Episodio: ${epi}` : file,
    season,
    epi, 
    ext,
  };
}

function buildPaths({ source, epi, season, ext }) {
  const SEASON = Number(season) < 10 ? `0${Number(season)}` : Number(season);
  const EPI = Number(epi) < 10 ? `0${Number(epi)}`: Number(epi);
  const renamedFile = `S${SEASON}E${EPI}${ext}`;
  return {
    source: source,
    dest: renamedFile,
  };
}

function doRename(folder, results) {
  results.forEach(rest => {
    const { source, dest } = buildPaths(rest);
    console.log((`'${folder}/${source}' ==> '${folder}/renamedFiles/${dest}'`));
    fs.copyFileSync(`${folder}/${source}`, `${folder}/renamedFiles/${dest}`);
  });
  
}

function showResults(results) {
  results.forEach(rest => {
    const { source, dest } = buildPaths(rest);
    console.log(`${source} => ${dest}`);
  });
}

function makeQuestion(folder, results) {
  const prompt = new Confirm({
    name: 'question',
    message: 'Do you want rename it?'
  });
  
  prompt.run().then(answer => answer && doRename(folder, results));
}

function main() {
  try {
    const folder = get(process, 'argv[2]');
    if (!folder) {
      console.error('No path argument, use: $> node index.js [path to folder]');
      return;
    }
    if (!sourceFolderExist(folder)) { 
      console.error(`Folder not found ${folder}`);
      return;
    }
    createFolderIfExist(folder);
    const results = fs.readdirSync(folder).map(martcherS03).filter(Boolean);
    if (results.length) {
      showResults(results);
      makeQuestion(folder, results);
    } else {
      console.log('Sorry, files not found');
    }
  } catch (error) {
    console.error(error);
  }
}

main();
